﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EjemploWebCoreEF.Models
{
    public partial class Modelos
    {
        public Modelos()
        {
            Repuestos = new HashSet<Repuestos>();
        }

        public int IdModelos { get; set; }
        public string Modelo { get; set; }
        public int? Asientos { get; set; }
        public string Combustible { get; set; }
        public decimal? Precio { get; set; }
        public string YearModelo { get; set; }
        public int? IdMarca { get; set; }

        public virtual Marca IdMarcaNavigation { get; set; }
        public virtual ICollection<Repuestos> Repuestos { get; set; }
    }
}
