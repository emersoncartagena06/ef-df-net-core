﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EjemploWebCoreEF.Models
{
    public partial class VwModelo
    {
        public int IdModelos { get; set; }
        public string Modelo { get; set; }
        public int? Asientos { get; set; }
        public string Combustible { get; set; }
        public decimal? Precio { get; set; }
        public string YearModelo { get; set; }
        public int? IdMarca { get; set; }
        public string Marca { get; set; }
        public string Pais { get; set; }
    }
}
