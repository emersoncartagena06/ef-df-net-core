﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EjemploWebCoreEF.Models
{
    public partial class Repuestos
    {
        public int IdRep { get; set; }
        public string Nombre { get; set; }
        public decimal? Precio { get; set; }
        public int? Descuento { get; set; }
        public int? IdModelos { get; set; }

        public virtual Modelos IdModelosNavigation { get; set; }
    }
}
