﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace EjemploWebCoreEF.Models
{
    public partial class autosContext : DbContext
    {
        public autosContext()
        {
        }

        public autosContext(DbContextOptions<autosContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Marca> Marca { get; set; }
        public virtual DbSet<Modelos> Modelos { get; set; }
        public virtual DbSet<Repuestos> Repuestos { get; set; }
        public virtual DbSet<VwModelo> VwModelo { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=(localdb)\\MSSQLLocalDB;Database=autos;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Marca>(entity =>
            {
                entity.HasKey(e => e.IdMarca)
                    .HasName("pk_marca");

                entity.ToTable("marca");

                entity.Property(e => e.IdMarca).HasColumnName("id_marca");

                entity.Property(e => e.Marca1)
                    .HasColumnName("marca")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Pais)
                    .HasColumnName("pais")
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Modelos>(entity =>
            {
                entity.HasKey(e => e.IdModelos)
                    .HasName("pk_modelos");

                entity.ToTable("modelos");

                entity.Property(e => e.IdModelos).HasColumnName("id_modelos");

                entity.Property(e => e.Asientos).HasColumnName("asientos");

                entity.Property(e => e.Combustible)
                    .HasColumnName("combustible")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.IdMarca).HasColumnName("id_marca");

                entity.Property(e => e.Modelo)
                    .HasColumnName("modelo")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Precio)
                    .HasColumnName("precio")
                    .HasColumnType("smallmoney");

                entity.Property(e => e.YearModelo)
                    .HasColumnName("year_modelo")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdMarcaNavigation)
                    .WithMany(p => p.Modelos)
                    .HasForeignKey(d => d.IdMarca)
                    .HasConstraintName("fk_modelos");
            });

            modelBuilder.Entity<Repuestos>(entity =>
            {
                entity.HasKey(e => e.IdRep)
                    .HasName("pk_repuesto");

                entity.ToTable("repuestos");

                entity.Property(e => e.IdRep).HasColumnName("id_rep");

                entity.Property(e => e.Descuento).HasColumnName("descuento");

                entity.Property(e => e.IdModelos).HasColumnName("id_modelos");

                entity.Property(e => e.Nombre)
                    .HasColumnName("nombre")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Precio)
                    .HasColumnName("precio")
                    .HasColumnType("smallmoney");

                entity.HasOne(d => d.IdModelosNavigation)
                    .WithMany(p => p.Repuestos)
                    .HasForeignKey(d => d.IdModelos)
                    .HasConstraintName("fk_repuestos");
            });

            modelBuilder.Entity<VwModelo>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_modelo");

                entity.Property(e => e.Asientos).HasColumnName("asientos");

                entity.Property(e => e.Combustible)
                    .HasColumnName("combustible")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.IdMarca).HasColumnName("id_marca");

                entity.Property(e => e.IdModelos).HasColumnName("id_modelos");

                entity.Property(e => e.Marca)
                    .HasColumnName("marca")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Modelo)
                    .HasColumnName("modelo")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Pais)
                    .HasColumnName("pais")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Precio)
                    .HasColumnName("precio")
                    .HasColumnType("smallmoney");

                entity.Property(e => e.YearModelo)
                    .HasColumnName("year_modelo")
                    .HasMaxLength(4)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
